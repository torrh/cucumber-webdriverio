var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {

    Given('I go to losestudiantes home screen', () => {

        browser.url('/');

        if (browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        browser.waitForVisible('button=Ingresar', 10000);
        browser.waitForEnabled('button=Ingresar', 10000);
        browser.click('button=Ingresar');
    });

    When('I fill a wrong email and password', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('wrongemail@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123467891')
    });

    Then('I expect to not be able to login', () => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    });


    When('I fill a correct email and password', () => {
        var cajaLogIn = browser.element('.cajaLogIn');

        // obtenemos el elemento de DOM para el mail
        var mailInput = cajaLogIn.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('ha.torres11@uniandes.edu.co');

        // obtenemos el elemento de DOM para el password
        var passwordInput = cajaLogIn.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123456789');
    });

    When('I try to login', () => {
        var cajaLogIn = browser.element('.cajaLogIn');
        cajaLogIn.element('.logInButton').click()
    });

    When('I expect be able to login', () => {
        browser.waitForVisible('#cuenta', 10000);
    });

    When('I fill the form to create an account not accept terms', () => {
        var cajaSignUp = browser.element('.cajaSignUp');

        //Obtener el elemento del DOM de nombre
        browser.waitForVisible('input[name="nombre"]', 10000);
        browser.waitForEnabled('input[name="nombre"]', 10000);
        var nameInput = cajaSignUp.element('input[name="nombre"]');
        nameInput.click();
        nameInput.keys('Alberto');

        //Obtener el elemento del DOM de apellido
        browser.waitForVisible('input[name="apellido"]', 10000);
        browser.waitForEnabled('input[name="apellido"]', 10000);
        var lastnameInput = cajaSignUp.element('input[name="apellido"]');
        lastnameInput.click();
        lastnameInput.keys('Torres');

        //Obtener el elemento del DOM de email
        browser.waitForVisible('input[name="correo"]', 10000);
        browser.waitForEnabled('input[name="correo"]', 10000);
        var mailInput = cajaSignUp.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('torrh85@gmail.com');

        browser.pause(5000)

        browser.waitForVisible('select[name="idUniversidad"]', 10000);
        browser.waitForEnabled('select[name="idUniversidad"]', 10000);
        var selectUniversity = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversity.selectByVisibleText("Universidad Nacional");

        browser.pause(5000)

        browser.waitForVisible('select[name="idPrograma"]', 10000);
        browser.waitForEnabled('select[name="idPrograma"]', 10000);
        var selectProgram = cajaSignUp.element('select[name="idPrograma"]');
        selectProgram.selectByVisibleText("Economía");

        browser.pause(5000)

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123456789');

        browser.pause(5000)

        browser.waitForVisible('input[name="acepta"]', 10000);
        browser.waitForEnabled('input[name="acepta"]', 10000);
        var checkAccept = cajaSignUp.element('input[name="acepta"]');
        checkAccept.click();
    });


    When('I open create account screen', () => {
        browser.waitForVisible('button=Ingresar', 10000);
        browser.waitForEnabled('button=Ingresar', 10000);
        browser.click('button=Ingresar');
    });


    When('I try to create account', () => {
        browser.waitForVisible('.logInButton', 10000);
        browser.waitForEnabled('.logInButton', 10000);
        var buttonAccept = cajaSignUp.element('.logInButton');
        buttonAccept.click();
    });

    Then('Then I expect to not be able to create account', () => {
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);
    });


    When('I fill the form to create an account short password', () => {
        var cajaSignUp = browser.element('.cajaSignUp');

        //Obtener el elemento del DOM de nombre
        browser.waitForVisible('input[name="nombre"]', 10000);
        browser.waitForEnabled('input[name="nombre"]', 10000);
        var nameInput = cajaSignUp.element('input[name="nombre"]');
        nameInput.click();
        nameInput.keys('Alberto');

        //Obtener el elemento del DOM de apellido
        browser.waitForVisible('input[name="apellido"]', 10000);
        browser.waitForEnabled('input[name="apellido"]', 10000);
        var lastnameInput = cajaSignUp.element('input[name="apellido"]');
        lastnameInput.click();
        lastnameInput.keys('Torres');

        //Obtener el elemento del DOM de email
        browser.waitForVisible('input[name="correo"]', 10000);
        browser.waitForEnabled('input[name="correo"]', 10000);
        var mailInput = cajaSignUp.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('torrh85@gmail.com');

        browser.pause(5000)

        browser.waitForVisible('select[name="idUniversidad"]', 10000);
        browser.waitForEnabled('select[name="idUniversidad"]', 10000);
        var selectUniversity = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversity.selectByVisibleText("Universidad Nacional");

        browser.pause(5000)

        browser.waitForVisible('select[name="idPrograma"]', 10000);
        browser.waitForEnabled('select[name="idPrograma"]', 10000);
        var selectProgram = cajaSignUp.element('select[name="idPrograma"]');
        selectProgram.selectByVisibleText("Economía");

        browser.pause(5000)

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123');

        browser.pause(5000)

        browser.waitForVisible('input[name="acepta"]', 10000);
        browser.waitForEnabled('input[name="acepta"]', 10000);
        var checkAccept = cajaSignUp.element('input[name="acepta"]');
        checkAccept.click();
    });

    When('Create account fail missing email field', () => {
        var cajaSignUp = browser.element('.cajaSignUp');

        //Obtener el elemento del DOM de nombre
        browser.waitForVisible('input[name="nombre"]', 10000);
        browser.waitForEnabled('input[name="nombre"]', 10000);
        var nameInput = cajaSignUp.element('input[name="nombre"]');
        nameInput.click();
        nameInput.keys('Alberto');

        //Obtener el elemento del DOM de apellido
        browser.waitForVisible('input[name="apellido"]', 10000);
        browser.waitForEnabled('input[name="apellido"]', 10000);
        var lastnameInput = cajaSignUp.element('input[name="apellido"]');
        lastnameInput.click();
        lastnameInput.keys('Torres');

        browser.pause(5000)

        browser.waitForVisible('select[name="idUniversidad"]', 10000);
        browser.waitForEnabled('select[name="idUniversidad"]', 10000);
        var selectUniversity = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversity.selectByVisibleText("Universidad Nacional");

        browser.pause(5000)

        browser.waitForVisible('select[name="idPrograma"]', 10000);
        browser.waitForEnabled('select[name="idPrograma"]', 10000);
        var selectProgram = cajaSignUp.element('select[name="idPrograma"]');
        selectProgram.selectByVisibleText("Economía");

        browser.pause(5000)

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123');

        browser.pause(5000)

        browser.waitForVisible('input[name="acepta"]', 10000);
        browser.waitForEnabled('input[name="acepta"]', 10000);
        var checkAccept = cajaSignUp.element('input[name="acepta"]');
        checkAccept.click();
    });

    When('I fill the form to create an account', () => {
        var cajaSignUp = browser.element('.cajaSignUp');

        //Obtener el elemento del DOM de nombre
        browser.waitForVisible('input[name="nombre"]', 10000);
        browser.waitForEnabled('input[name="nombre"]', 10000);
        var nameInput = cajaSignUp.element('input[name="nombre"]');
        nameInput.click();
        nameInput.keys('Alberto');

        //Obtener el elemento del DOM de apellido
        browser.waitForVisible('input[name="apellido"]', 10000);
        browser.waitForEnabled('input[name="apellido"]', 10000);
        var lastnameInput = cajaSignUp.element('input[name="apellido"]');
        lastnameInput.click();
        lastnameInput.keys('Torres');

        //Obtener el elemento del DOM de email
        browser.waitForVisible('input[name="correo"]', 10000);
        browser.waitForEnabled('input[name="correo"]', 10000);
        var mailInput = cajaSignUp.element('input[name="correo"]');
        mailInput.click();
        mailInput.keys('torrh85@gmail.com');

        browser.pause(5000)

        browser.waitForVisible('select[name="idUniversidad"]', 10000);
        browser.waitForEnabled('select[name="idUniversidad"]', 10000);
        var selectUniversity = cajaSignUp.element('select[name="idUniversidad"]');
        selectUniversity.selectByVisibleText("Universidad Nacional");

        browser.pause(5000)

        browser.waitForVisible('select[name="idPrograma"]', 10000);
        browser.waitForEnabled('select[name="idPrograma"]', 10000);
        var selectProgram = cajaSignUp.element('select[name="idPrograma"]');
        selectProgram.selectByVisibleText("Economía");

        browser.pause(5000)

        var passwordInput = cajaSignUp.element('input[name="password"]');
        passwordInput.click();
        passwordInput.keys('123456789');

        browser.pause(5000)

        browser.waitForVisible('input[name="acepta"]', 10000);
        browser.waitForEnabled('input[name="acepta"]', 10000);
        var checkAccept = cajaSignUp.element('input[name="acepta"]');
        checkAccept.click();
    });

    Then('Then I expect to be able to create account', () => {
        browser.waitForVisible('.sweet-alert ', 5000);
    });
});