Feature: Create account into losestudiantes
  As an user I want to create an account within losestudiantes website in order to rate

  Scenario: Create account fail by not accept terms
    Given I go to losestudiantes home screen
    When I open create account screen
    And I fill the form to create an account not accept terms
    And I try to create account
    And Then I expect to not be able to create account

  Scenario: Create account fail by short password
    Given I go to losestudiantes home screen
    When I open create account screen
    And I fill the form to create an account short password
    And I try to create account
    And Then I expect to not be able to create account

  Scenario: Create account fail missing email field
    Given I go to losestudiantes home screen
    When I open create account screen
    And I fill the form to create an account missing email field
    And I try to create account
    And Then I expect to not be able to create account

  Scenario: Create account succesful
    Given I go to losestudiantes home screen
    When I open create account screen
    And I fill the form to create an account
    And I try to create account
    And Then I expect to be able to create account

